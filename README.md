# Programação Modular - 2022.2

## Grupo

- Cassiano Junior
- Maria Oliveira
- Vítor Barroso

## Organização dos Arquivos

As entregas estão organizadas em pastas dentro do diretório 'entregas'.

O arquivo Swagger, 'swagger.yaml', na pasta 'docs' é sempre atualizado conforme o andamento da disciplina.
